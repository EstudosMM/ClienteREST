package br.com.clienterest.model.dao;

import br.com.clienterest.model.entity.Cliente;

/**
 * DAO para cliente
 * 
 * @author marcelomichels
 *
 */
public class ClienteDAO extends DAO<Cliente>{
	public ClienteDAO() {
		setCollecion("cliente");
		setClazz(Cliente.class);
	}
}
