package br.com.clienterest.model.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.clienterest.model.entity.EntityScope;
import br.com.clienterest.model.factory.MongoDataBase;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

/**
 * Contem todas as regras dinamicas de DAO
 *
 * @author Marcelo Michels
 * 
 */
public class DAO<Entity extends EntityScope>{
	
	private Class<Entity> clazz;
	
	private MongoDataBase mongo = new MongoDataBase();
	
	private String collecion;

	public void salvar(Entity e) throws Exception {
		if (e.get_id() <= 0) {
			e.set_id(getNextId());
			inserir(e);
		} else {
			BasicDBObject searchQuery = new BasicDBObject("_id", e.get_id());
			atualizar(searchQuery, e);
		}
	}

	protected void setCollecion(String collecion) {
		this.collecion = collecion;
	}

	protected DBCollection getCollecion() throws Exception{
		return getMongo().getDB().getCollection(collecion);
	}

	public void inserir(Object o) throws Exception {
		final Gson g = new Gson();
		final BasicDBObject doc = (BasicDBObject) JSON.parse(g.toJson(o));
		insert(doc);
	}

	public void atualizar(BasicDBObject searchQuery, Object o) throws Exception {
		final Gson g = new Gson();
		final BasicDBObject doc = (BasicDBObject) JSON.parse(g.toJson(o));
		update(searchQuery, doc);
	}

	protected MongoDataBase getMongo() {
		return mongo;
	}

	private void insert(BasicDBObject doc) throws Exception {
		getMongo().insert(getCollecion(), doc);
	}

	private void update(BasicDBObject searchQuery, BasicDBObject doc)
			throws Exception {
		getMongo().update(getCollecion(), searchQuery, doc);
	}

	public void removerById(long id) throws Exception{
		DBObject remove = new BasicDBObject("_id", id);
		getCollecion().remove(remove);
	}

	public List<Entity> getAll() throws Exception{
		ArrayList<Entity> all = new ArrayList<Entity>();
		Gson gson = new Gson();
		DBCursor cur = getCollecion().find();
		while (cur.hasNext()) {
			Entity o = gson.fromJson(cur.next().toString(), clazz);
			all.add(o);
		}
		return all;
	}

	public Entity getById(long id) throws Exception{
		Entity o = null;
		Gson gson = new Gson();
		BasicDBObject search = new BasicDBObject("_id", id);
		DBCursor cur = getCollecion().find(search);
		if (cur.hasNext()) {
			o = gson.fromJson(cur.next().toString(), clazz);
		}
		return o;
	}

	protected long getNextId() throws Exception{
        long id = 0;
        DBObject sort = new BasicDBObject();
        sort.put("_id", -1);

        DBCursor cursor = getCollecion().find().sort(sort).limit(1);

        if (cursor.hasNext()) {
            Gson gson = new Gson();
            String json = cursor.next().toString();
            Entity e = gson.fromJson(json, clazz);
            id = e.get_id();
        }
        return ++id;
	}

	public void setClazz(Class<Entity> clazz) {
		this.clazz = clazz;
	}	
}
