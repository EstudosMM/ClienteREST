package br.com.clienterest.model.factory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;

/**
 * Obejeto de conexao com o banco de dados MongoDB
 * 
 * @author marcelomichels
 *
 */
public class MongoDataBase {

	private static final DB db;

	static {
		try {
			final Properties prop = new Properties();
			final InputStream input = MongoDataBase.class
					.getResourceAsStream("mongo.properties");

			prop.load(input);

			@SuppressWarnings("deprecation")
			final Mongo m = new Mongo(prop.getProperty("host"),
					Integer.valueOf(prop.getProperty("port")));
			db = m.getDB(prop.getProperty("database"));
		} catch (Exception e) {
			throw new ExceptionInInitializerError(
					"Impossivel conectar ao banco de dados");
		}
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("Criou MongoDataBase");
	}

	public DB getDB() {
		return db;
	}

	public void insert(final DBCollection coll, final BasicDBObject doc)
			throws Exception {
		coll.insert(doc);
	}

	public void update(final DBCollection coll,
			final BasicDBObject searchQuery, final BasicDBObject doc)
			throws Exception {
		coll.update(searchQuery, doc);
	}
}
