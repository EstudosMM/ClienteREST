package br.com.clienterest.model.entity;


/**
 * Entidade que é escopo para todas as demais 
 * 
 * @author marcelomichels
 *
 */
public interface EntityScope {

	long _id = 0;
	
	public long get_id();
	public void set_id(long _id);
}