package br.com.clienterest.model.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Obejeto que represanta um cliente
 * 
 * @author marcelomichels
 *
 */
@XmlRootElement
public class Cliente implements EntityScope{
	
	long _id;
	String nome;
	String cpf;
	String sexo;
	String telefone;
	int idade;
	String endereco;
	
	public Cliente() {
	
	}

	@Override
	public long get_id() {
		return _id;
	}

	@Override
	public void set_id(long _id) {
		this._id = _id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

}
