package br.com.clienterest.controller;

import java.util.List;

import br.com.clienterest.model.dao.ClienteDAO;
import br.com.clienterest.model.entity.Cliente;

/**
 * Disponibiliza as operações de um cliente
 * 
 * @author marcelomichels
 *
 */
public class ClienteController {
	private ClienteDAO dao = new ClienteDAO();

	public List<Cliente> getAll() throws Exception {
		return dao.getAll();
	}

	public Cliente getById(long id) throws Exception {
		return dao.getById(id);
	}

	public void salvar(Cliente cliente) throws Exception {
		dao.salvar(cliente);
	}
	
	public void removerById(long id) throws Exception {
		dao.removerById(id);
	}	
}