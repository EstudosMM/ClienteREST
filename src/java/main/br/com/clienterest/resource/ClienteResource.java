package br.com.clienterest.resource;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.clienterest.controller.ClienteController;
import br.com.clienterest.model.entity.Cliente;

import com.google.gson.Gson;

@Path(ClienteResource.ENDPOINT)
public class ClienteResource {

	static final String ENDPOINT = "cliente";
	
	static final Logger log = Logger.getLogger(ClienteResource.class.getName());
	
	static Gson gson = new Gson();
	
	ClienteController clienteController = new ClienteController(); 
	
	@POST
	@Path("salvar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response post(Cliente cliente) {
		
		try {
			
			log.info(ENDPOINT + " -> @POST -> Inicio");
			
			clienteController.salvar(cliente);
			
			log.info(ENDPOINT + " -> @POST -> Fim");
			
			return Response.ok().build();
			
		} catch (Exception e) {
			log.info(ENDPOINT + " -> @GET -> Exception");
			e.printStackTrace();
			return Response.serverError().build();
		}
	}
	
	@POST
	@Path("buscar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(){
		
		Response response = null;
		
		log.info(ENDPOINT + " -> @GET -> Inicio");
		
		try {
			
			response = Response.ok(clienteController.getAll()).build();
			
		} catch (Exception e) {
			log.info(ENDPOINT + " -> @GET -> Exception");
			e.printStackTrace();
			response =  Response.serverError().build();
		}
		
		log.info(ENDPOINT + " -> @GET -> Fim");
		
		return response;
	}
	
	@POST
	@Path("deletar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response delete(Cliente cliente) {
		
		try {
			
			log.info(ENDPOINT + " -> @DELETE -> Inicio");
			
			clienteController.removerById(cliente.get_id());
			
			log.info(ENDPOINT + " -> @DELETE -> Fim");
			
			return Response.ok().build();
			
		} catch (Exception e) {
			log.info(ENDPOINT + " -> @DELETE -> Exception");
			e.printStackTrace();
			return Response.serverError().build();
		}
	}	
}