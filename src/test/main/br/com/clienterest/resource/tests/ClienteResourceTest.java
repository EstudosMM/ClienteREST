package br.com.clienterest.resource.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import br.com.clienterest.model.entity.Cliente;
import br.com.clienterest.resource.conf.GenericTest;

import com.google.gson.reflect.TypeToken;

public class ClienteResourceTest extends GenericTest {

	public static void main(String[] args) {
		new ClienteResourceTest().inserir();
	}

	@Test
	public void inserir() {

		Cliente cliente = new Cliente();
		cliente.setNome("Marcelo Michels");
		cliente.setSexo("M");
		cliente.setTelefone("4699727256");
		cliente.setCpf("08002956923");
		cliente.setEndereco("Rua capanema, 1330");
		cliente.setIdade(20);

		Entity<String> entity = Entity.entity(g.toJson(cliente),
				MediaType.APPLICATION_JSON);

		Response response = target().path("cliente").request().post(entity);
		assertEquals(200, response.getStatus());
	}

	@Test
	public void buscarTodos() {

		String clientes = target().path("cliente").request().get(String.class);
		List<Cliente> cliente = g.fromJson(clientes,
				new TypeToken<List<Cliente>>() {
				}.getType());
		assertTrue(cliente.size() > 0);
	}
}
