package br.com.clienterest.resource.conf;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class TestConfi { 
	
	public static WebTarget getWebTarget() {
		
		Client client = ClientBuilder.newClient();
		
		return client.target("http://localhost:8080/ClienteREST/api");
		//return client.target("http://marcelomichels.com/ClienteREST/api");
	}
}