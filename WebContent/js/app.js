

var myApp = angular.module('ClienteApp',[]);

myApp.controller('ClienteController', ['$scope', '$http', function($scope, $http) {

    var URL = "http://" + location.host + "/ClienteREST/api/cliente";

    $scope.init = function () {
        listarClientes();
    };    

    function listarClientes() { 
        $http.post(URL + "/buscar")
        .success(function(data, status, headers, config) {
            $scope.clientes = data;
        })
        .error(function(data, status, headers, config) {
            $scope.error = status;
        });
    }

    $scope.editar = function (cliente) {
        $scope.cliente = cliente;
    };

    $scope.salvar = function () {
        $http.post(URL + "/salvar", $scope.cliente)
        .success(function(data, status, headers, config) {
            console.log("salvar ->  sucesso");
            $scope.cliente = null;
            listarClientes();
        })
        .error(function(data, status, headers, config) {
            console.log("salvar ->  error -> status -> " + status);
        });
    };

    $scope.deletar = function (cliente) {
        $http.post(URL + "/deletar", {_id:cliente._id})
        .success(function(data, status, headers, config) {
            console.log("deletar -> success");
            listarClientes();
        })
        .error(function(data, status, headers, config) {
            console.log("deletar ->  error -> status -> " + status);
        });
    };      

}]);